import requests
from lxml import etree
# 浏览器伪装
ua = 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko'
header = {"User-Agent": ua}
# GET请求
url = 'http://www.runoob.com/ajax/ajax-database.html'
r = requests.get(url, headers=header)
r.encoding = 'urf-8'
text = r.text
# xpath提取信息
element = etree.HTML(text)
options = element.xpath('/html/body/div[3]/div/div[2]/div/div[3]/div/div[1]/div/form/select/option/@value')
print(options)
# 提取异步加载的信息
for option in options:
    url = 'http://www.runoob.com/try/ajax/getcustomer.php?q='+option.strip()
    r = requests.get(url, headers=header)
    r.encoding = 'GBK'
    text = r.text
    # xpath提取信息
    element = etree.HTML(text)
    em = element.xpath('/html/body/table/tr[1]/td[1]/em/text()')
    td = element.xpath('/html/body/table/tr[1]/td[2]/text()')
    for e, t in zip(em, td):
        print(e, ' | ', t)
    print('-' * 55, options.index(option) + 1)