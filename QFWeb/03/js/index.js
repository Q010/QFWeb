var docCategory = null,
    docCatalog = null,
    docArea = null,
    drawingCategory = null,
    drawingCatalog = null,
    blogTags = null,
    blogTimespan = null,
    pager = null,
    valid = /[<>]+/;

function customTagDialog(callback) {
    // layer.prompt(cfg, callback);
    layer.open({
        title: '自定义标签',
        type: 1,
        content: $('#custom_tags'),
        btn: ['确定'],
        cancel: function() { callback(''); },
        yes: function(index, layerpo) {
            var tag = $('#tag_input', layerpo).val().trim();
            if (tag.length <= 0) {
                layer.msg('标签不能为空！');
            } else if (valid.test(tag)) {
                layer.msg('标签中包含特殊字符！');
            } else {
                callback(tag)
                layer.close(index);
            }
        }
    });
};

function timeSpan(callback) {
    layer.open({
        title: '选择时间范围',
        type: 1,
        content: $('#timeSpan'),
        btn: ['确定'],
        move: false,
        cancel: function() {
            callback([]);
            $('.layui-laydate').hide();
        },
        yes: function(index, layerpo) {
            var s = $('#sdate', layerpo).val(),
                e = $('#edate', layerpo).val(),
                sd = new Date(s),
                se = new Date(e);
            if (sd > se) {
                layer.msg('开始时间需小于截止时间');
                return;
            }
            callback([s, e]);
            layer.close(index);
        }
    });
};

$(function() {

    layui.use('laydate', function() {
        var laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#sdate', //指定元素
            type: 'date',
            value: new Date().toLocaleDateString(),
            format: 'yyyy-MM-dd',
            showBottom: false
        });
        //执行一个laydate实例
        laydate.render({
            elem: '#edate', //指定元素
            type: 'date',
            value: new Date().toLocaleDateString(),
            format: 'yyyy-MM-dd',
            showBottom: false
        });
    });

    $('.z_tab .tab_item').click(function() {
        var target = $(event.srcElement || event.target);
        if (target.is('.sel')) return;
        else {
            var con = $('.z_tab'),
                tabs = $('.tab_item', con),
                contents = $('.tab_content', con);
            tabs.removeClass('sel');
            target.addClass('sel');
            contents.hide();
            module = target.attr('data-value');
            $.each(contents, function(i, item) {
                if ($(item).attr('data-value') == module)
                    $(item).show();
            });
            //$('.tab_content[data-value="' + module + '"]', con).show();
        }
    });

    docCategory = $('#doc_category').dropdown({
        click: function(sid) {
            docCatalogBind(sid, 0);
            docAreaBind(sid, 0);
        }
    });
    docCatalog = $('#doc_catalog').dropdown();

    if (module == 'criterion') {
        docCategory.select(categoryId);
        docCatalogBind(categoryId, catalogs);
    } else {
        docCategory.select(0);
        docCatalogBind(0, '');
    }
    docArea = $('#doc_area').dropdown();
    docAreaBind(categoryId, areaId);

    drawingCategory = $('#drawing_category').dropdown({
        click: function(sid) {
            drawingCatalogBind(sid, 0);
        }
    });
    drawingCatalog = $('#drawing_catalog').dropdown();

    if (module == 'drawing') {
        drawingCategory.select(categoryId);
        drawingCatalogBind(categoryId, catalogs);
    } else {
        drawingCategory.select(0);
        drawingCatalogBind(0, '');
    }

    blogTags = $('#blog_tags').dropdown({
        click: function(sid) {
            if (sid == -1) {
                customTagDialog(function(value) {
                    if (value == '')
                        blogTags.select(0);
                    else {
                        blogTags.setText(value);
                    }
                    tags = value;
                });
                //blogTags.setText(value);
                blogTags.select(0);
            }
        }
    });
    if (tags != '')
        blogTags.setText(tags);
    else
        blogTags.select(0);

    blogTimespan = $('#blog_timespan').dropdown({
        click: function(sid) {
            timespan = '';
            if (sid == 9) {
                timeSpan(function(data) {
                    if (data.length <= 0) {
                        blogTimespan.select(0);
                    } else {
                        blogTimespan.select(-1);
                        timespan = data[0] + '至' + data[1];
                        blogTimespan.setText('自定义(' + timespan + ')');
                    }
                });
            }
            var date = new Date();
            if (sid == 1) {
                timespan = date.format('yyyy-MM-dd');
                blogTimespan.setText('今日(' + timespan + ')');

            } else if (sid == 2) {
                date.setDate(date.getDate() - 1);
                timespan = date.format('yyyy-MM-dd')
                blogTimespan.setText('昨日(' + timespan + ')');
            } else if (sid == 3) {
                date.setDate(date.getDate() - 2)
                timespan += date.format('yyyy-MM-dd') + '至';
                date.setDate(date.getDate() + 2);
                timespan += date.format('yyyy-MM-dd');
                blogTimespan.setText('三日(' + timespan + ')');
            } else if (sid == 4) {
                date.setDate(date.getDate() - 6)
                timespan += date.format('yyyy-MM-dd') + '至';
                date.setDate(date.getDate() + 6);
                timespan += date.format('yyyy-MM-dd');
                blogTimespan.setText('一周(' + timespan + ')');
            } else if (sid == 5) {
                date.setMonth(date.getMonth() - 1)
                timespan += date.format('yyyy-MM-dd') + '至';
                date.setMonth(date.getMonth() + 1);
                timespan += date.format('yyyy-MM-dd');
                blogTimespan.setText('一月(' + timespan + ')');
            } else if (sid == 6) {

                date.setMonth(date.getMonth() - 3)
                timespan += date.format('yyyy-MM-dd') + '至';
                date.setMonth(date.getMonth() + 3);
                timespan += date.format('yyyy-MM-dd');
                blogTimespan.setText('三月(' + timespan + ')');
            } else if (sid == 7) {
                date.setMonth(date.getMonth() - 6)
                timespan += date.format('yyyy-MM-dd') + '至';
                date.setMonth(date.getMonth() + 6);
                timespan += date.format('yyyy-MM-dd');
                blogTimespan.setText('半年(' + timespan + ')');
            } else if (sid == 8) {
                date.setFullYear(date.getFullYear() - 1);
                timespan += date.format('yyyy-MM-dd') + '至';
                date.setFullYear(date.getFullYear() + 1);
                timespan += date.format('yyyy-MM-dd');
                blogTimespan.setText('一年(' + timespan + ')');
            }
        }
    });

    blogTimespanBind(timespan);

    $('.button').click(function() {
        var target = $(event.target || event.srcElement),
            body = target.parents('.tab_content');
        module = body.attr('data-value');
        categoryId = 0;
        catalogs = '';
        areaId = 0,
            tags = '',
            timespan = '';
        page = 1;
        keyword = $('.tab_content[data-value="' + module + '"] #keyword').val().trim();
        if (keyword.length <= 0) {
            layer.msg('请输入搜索关键字！');
            return;
        }
        if (module == 'criterion') {
            type = $('[name="doctype"]:checked').val();
            categoryId = docCategory.getSelect();
            catalogs = docCatalog.getSelect();
            areaId = docArea.getSelect();
        } else if (module == 'drawing') {
            type = $('[name="drawingtype"]:checked').val();
            categoryId = drawingCategory.getSelect();
            catalogs = drawingCatalog.getSelect();
        } else if (module == 'blog') {
            type = $('[name="blogtype"]:checked').val();
            tags = blogTags.getText();
            //timespan = blogTimespan.getText().replace('(', '').replace(')', '').replace('至', '-');
            timespan = blogTimespan.getText();
        }

        if (valid.test(keyword)) {
            layer.msg('关键字包含非法字符！');
            return;
        }

        location.href = '/sou/?module=' + module +
            '&keyword=' + escape(keyword) +
            '&type=' + type +
            '&categoryId=' + categoryId +
            '&catalogs=' + catalogs +
            '&areaId=' + areaId +
            '&tags=' + escape(tags) +
            '&timespan=' + escape(timespan);
    });

    $('.right .list').on('click', '.author', function() {
        var target = $(event.target || event.srcElement),
            p = target.parents('.item'),
            author = p.data('book').BlogId;
        window.open(blogUrl + '/b/' + author, '_blank');
    });

    var sbody = $('.tab_content[data-value="' + module + '"]');
    keyword = $('#keyword', sbody).val().trim();

    if (keyword != '') {
        dataBind(page);
    }

    $('[name="drawingtype"]').change(function() {
        var target = $(event.target || event.srcElement),
            type = target.val();
        drawingCategoryBind(type);
        drawingCatalogBind(0, '');
    });

    $('.list').on('click', '.title', function() {
        var target = $(event.target || event.srcElement),
            p = target.parents('.item'),
            order = $('.right .list .item').index(p) + 1,
            page = pager.getPage(),
            identify = cookie.get('identify'),
            bookId = p.data('book').Sid,
            chapterId = p.data('book').ChapterId;

        $.ajax({
            url: '/sou/home/appendsearchoperate',
            data: {
                identify: identify,
                type: 1,
                datasource: 1,
                page: page,
                order: order,
                bookId: bookId,
                chapterId: chapterId
            },
            method: 'POST',
            success: function(or) {

            },
            error: function(or) {
                //layer.msg(or.statusText);
            }
        });
    });

    $(document).keypress(function() {
        if (event.keyCode == 13) {
            var active = $(document.activeElement);
            if (active.is('.tab_content[data-value="' + module + '"] #keyword')) {
                $('.tab_content[data-value="' + module + '"] .button').click();
            }
        }
    });

    $('.tab_item').click(function() {
        var sm = $(this).attr('data-value'),
            key = sm + '_url';
        if (sm == request('module')) return;
        if (cookie.get(key))
            location.href = cookie.get(key);
    });
});

function docCatalogBind(categoryId, selectId) {
    docCatalog.bind('/sou/home/catalogs', { parentId: categoryId }, function() {
        docCatalog.prepend('全部', 0);
        docCatalog.select(selectId == '' ? 0 : selectId);
    });
};

function drawingCategoryBind(type) {
    drawingCategory.bind('/sou/home/DrawingCategorys', { type: type }, function() {
        drawingCategory.prepend('全部', 0);
        drawingCategory.select(0);
    });
};

function drawingCatalogBind(categoryId, selectId) {
    drawingCatalog.bind('/sou/home/DrawingCatalogs', { type: type, parentId: categoryId }, function() {
        drawingCatalog.prepend('全部', 0);
        drawingCatalog.select(selectId == '' ? 0 : selectId);
    });
};

function docAreaBind(categoryId, selectId) {
    docArea.bind('/sou/home/areas', { categoryId: categoryId }, function() {
        docArea.prepend('全部', 0);
        docArea.select(selectId == '' ? 0 : selectId);
    });
};

function blogTagsBind(tags) {
    blogTags.bind('/sou/home/tags', null, function() {
        blogTags.prepend('全部', "");
        blogTags.select(selectId == '' ? 0 : selectId);
    });
};

function blogTimespanBind(timespan) {
    blogTimespan.bind([
        { Sid: '0', Name: '全部' },
        { Sid: '1', Name: '今日' },
        { Sid: '2', Name: '昨日' },
        { Sid: '3', Name: '三日' },
        { Sid: '4', Name: '一周' },
        { Sid: '5', Name: '一月' },
        { Sid: '6', Name: '三月' },
        { Sid: '7', Name: '半年' },
        { Sid: '8', Name: '一年' },
        { Sid: '9', Name: '自定义' }
    ]);
    if (timespan != '') {
        blogTimespan.setText(timespan);
    } else
        blogTimespan.select(0);
};

function dataBind(page) {
    var span = timespan.match(/\d+-\d+-\d+/g),
        container = $('.tab_content[data-value="' + module + '"]');
    $('.cover', container).show();
    $('.no-data', container).hide();
    $.post('/sou/home/sitesearch', { module: module, categoryId: categoryId, keyword: keyword, catalogs: catalogs, searchType: type, areaId: areaId, tags: tags, timespan: span, page: page },
        function(or) {
            var list = $('.right .list', container),
                tmp = $(' > *', list).remove();
            tmp = null;
            if (or.PageCount > 0) {
                for (var i = 0; i < or.Datas.length; i++) {
                    var data = or.Datas[i],
                        item = '',
                        url = '#',
                        bookId = data.Sid,
                        chapterId = data.ChapterId;
                    //页面跳转
                    if (module == 'criterion' && data.Type != 2) {
                        if (data.ResourceType == 1)
                            url = criterionUrl + '/webarbs/book/' + bookId + (chapterId == 0 ? '.shtml' : '/' + chapterId + '.shtml');
                        else if (data.ResourceType == 2)
                            url = criterionUrl + '/webarbs/publicnoticeinfo/' + bookId + '.shtml';
                    } else if (module == 'blog')
                        url = blogUrl + '/b/u/article/' + bookId;
                    else if (module == 'drawing')
                        url = drawingUrl + '/tzmx/tzpage/' + bookId + '.aspx';

                    if (module == 'criterion') {
                        var html = '<div class="item">{0}<a class="{1}" title="{2}" href="{3}" target="{4}">{2}</a><div class="content">{5}</div><div class="catalog"> {6}{7}{8}</div></div>'.format((data.Abolish ? '<label class="abolish">废止</label>' : ''), (data.Type == 2 ? 't-title' : 'title'), data.Title, url, (data.Type == 2 ? '_self' : '_blank'), data.Content, data.CategoryName, (data.Area ? ' > ' + data.Area : ''), (data.Catalogs ? ' > ' + data.Catalogs : ''));
                        item = $(html);
                    } else if (module == 'drawing') {
                        item = $('<div class="item"><div class="thumb"><img src="{0}" onerror="this.src=\'/sou/content/image/none.jpg\';" /></div><div class="text"><div class="row"><a target="_blank" class="title" href="{1}">{2}</a></div><div class="row content" style="min-height:4.25em;padding:5px 0;">{3}</div><div class="row catalog">{4}> {5}</div></div></div>'.format(data.Thumb, url, data.Title, data.Content.ellipsis(132), data.CategoryName, data.Catalogs));
                    } else {
                        item = $('<div class="item"><a class="title" title="' + data.Title + '" href="' + url + '" target="_blank">' + data.Title + '</a><div class="content">' + data.Content + '</div><div class="catalog">作者：<a class="author" href="#">' + data.Author + '</a>&nbsp;标签： ' + data.Tags + '&nbsp;发表时间:' + data.CreateTime + '</div></div>');
                    }
                    item.data('book', data);
                    list.append(item);
                }
            } else {
                $('.no-data', container).show();
            }
            pager = $('.right .pager', container).pager({
                pageCount: or.PageCount,
                click: function(page) {

                    location.href = '/sou/?module=' + module +
                        '&keyword=' + escape(keyword) +
                        '&type=' + type +
                        '&categoryId=' + categoryId +
                        '&catalogs=' + catalogs +
                        '&areaId=' + areaId +
                        '&tags=' + escape(tags) +
                        '&timespan=' + escape(timespan) +
                        '&page=' + page;
                },
                span: 10,
                initPage: page
            });

            for (var i = 0; i < or.Keywords.length; i++) {
                $('.title', list).highlight(or.Keywords[i]);
                $('.content', list).highlight(or.Keywords[i]);
            }
            $('.cover', container).hide();
        }, 'json');
};