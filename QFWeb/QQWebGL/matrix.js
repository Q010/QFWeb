function translate(Tx, Ty, Tz) {
    return new Float32Array([
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        Tx, Ty, Tz, 1.0,
    ])
}

function scale(Tx, Ty, Tz) {
    return new Float32Array([
        Tx, 0.0, 0.0, 0.0,
        0.0, Ty, 0.0, 0.0,
        0.0, 0.0, Tz, 0.0,
        0.0, 0.0, 0.0, 1.0,
    ])
}

function rotate(angle) {
    let sinB = Math.sin(Math.PI * angle / 180.0);
    let cosB = Math.cos(Math.PI * angle / 180.0);
    return new Float32Array([
        cosB, sinB, 0.0, 0.0,
        -sinB, cosB, 0.0, 0.0,
        0.0, 0.0, 1, 0.0,
        0.0, 0.0, 0.0, 1.0,
    ])
}
