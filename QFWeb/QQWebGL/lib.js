function initShader(gl,VERTEX_SHADER,FRAG_SHADER) {
    let vertex=gl.createShader(gl.VERTEX_SHADER);//创建顶点着色器
    let frag=gl.createShader(gl.FRAGMENT_SHADER);//创建片元着色器

    gl.shaderSource(vertex,VERTEX_SHADER);//指定点着色器对象
    gl.shaderSource(frag,FRAG_SHADER);//指定片元着色器对象

    gl.compileShader(vertex);//编译顶点着色器
    gl.compileShader(frag);//编译片元着色器

    let program=gl.createProgram();//创建程序对象
    gl.attachShader(program,vertex);//为程序对象分配顶点着色器
    gl.attachShader(program,frag);//为程序对象分配片元着色器

    gl.linkProgram(program);//链接程序对象

    gl.useProgram(program);//告知使用的对象
    // gl.clearColor(0.4,0.5,0.6,1.0);
    // gl.clear(gl.COLOR_BUFFER_BIT);
    return program;
}

function bufferInit(gl,dataVertices,pt,program) {
    let buffer=gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,buffer);
    gl.bufferData(gl.ARRAY_BUFFER,dataVertices,gl.STATIC_DRAW);
    let a_pos=gl.getAttribLocation(program,pt);
    gl.vertexAttribPointer(a_pos,2,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(a_pos);
}