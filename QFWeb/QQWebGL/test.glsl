attribute vec4 a_pos;
attribute vec4 a_color;
varying vec4 v_color;
uniform mat4 u_ViewMatrix;
void main(){
    gl_Position=u_ViewMatrix*a_pos;
    v_color=a_color;
}

precision lowp float;
uniform sampler2D u_Sampler;
varying mat4 u_ViewMatrix;
void main(){
    gl_FragColor=v_color;
}