import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import params from '@/components/params'
import Hi1 from '@/components/Hi1'
import Error from '@/components/Error'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [{
            path: '/params/:newsId(\\d+)/:newsTitle',
            component: params,
            beforeEnter: (to, from, next) => {
                console.log(to);
                console.log(from);
                // console.log(next);
                next();

            }
        },
        {
            path: "/",
            name: 'hello',
            component: HelloWorld,
            alias: 'home1'
        },
        {
            path: '/goHome',
            redirect: '/'
        },
        {
            path: '/goparams/:newsId(\\d+)/:newsTitle',
            redirect: '/params/:newsId(\\d+)/:newsTitle'
        },
        {
            path: '/Hi1',
            component: Hi1,
            alias: '/Hhh',
        }, {
            path: '*',
            component: Error,
        }
    ]
})