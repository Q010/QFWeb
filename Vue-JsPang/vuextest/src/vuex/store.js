import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
const state = {
    count: 1,
}
const mutations = {
    add(state, num) {
        state.count += num;
    },
    reduce(state, num) {
        state.count -= num;
    }
}
const getters = {
    count: function(state) {
        return state.count += 20;
    }
}
const actions = {
    addAction(context) {
        context.commit('add', 10);
    },
    reduceAction({ commit }) {
        commit('reduce', 5);
    }
}
export default new Vuex.Store({
    state,
    mutations,
    getters,
    actions
})